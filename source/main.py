# -*- coding: utf-8 -*-

def ParserLet(lett, offset):
    # Расчет последовательности листов внутри тетради
    i=1
    ItogTetr = []
    while i <= lett/2:
         print\
            offset + lett - (i-1), ", " ,\
            offset + i, ", " ,\
            offset + i+1, ", " ,\
            offset + lett - i
         ItogTetr = ItogTetr + [offset + lett - (i-1), offset + i, offset + i+1, offset + lett - i]
         i=i+2
    return ItogTetr

def ParseTetr(lett):
    #Разбивка потетрадная
    print "Попытка деления по тетрадям"
    print "листов в тетради - к-во тетрадей - листов не вошло в тетрадь"
    for i in xrange(2, 12):
        dev = divmod(lett, i)
        if dev[0] == 0: break
        print i, " - ",  dev[0], " - ",  dev[1]
    print "Какое количество листов в тетради использовать?"
    lists = int(raw_input())                                                                  #Количество листов в тетради
    dev = divmod(lett, lists)
    int_etr = dev[0]                                                                          #Количество тетрадей
    out_tetr = dev[1]                                                                         #Количество листов не вошедших в тетради
    return (int_etr, out_tetr, lists)

def AddLett(lett):
    #Разбивка постраничная
    #Проверка что количество делится на 4 и дополнение их числа до делимого
    kratnoe = divmod(lett, 4)
    print kratnoe[1]
    if kratnoe[1] != 0:
        print "Произведено дополнение числа страниц"
        addLet = 4-kratnoe[1]                                                                 #Количество добавляемых страниц
        lett = lett + addLet
    return (lett, addLet)
     
def main():
    # Количество страниц
    lett = 17                                                                                 #Количество страниц изначально
    addLet = 0
    Itog = []
    
    print "Исходное количество страниц: ", lett
    lett, addLet = AddLett(lett)
    print "Количество страниц для печати: ", lett
    print "Страниц было добавлено: ",  addLet
    list = lett/4
    print "Листов для печати: ",  list                                                        #Количество листов для печати
    
    int_etr, out_tetr, lists = ParseTetr(list)
    print "Количество листов в тетради: ", lists
    print "Количество тетрадей: ", int_etr
    print "Количество листов (не страниц) не вошедших в тетради: ", out_tetr

    print
    print "Разбиение страниц по листам (указаны в строку) и тетрадям", out_tetr
    tetra = 0
    while tetra < int_etr:
        print "Тетрадь №", tetra
        offset = tetra * lists * 4
        print "Смещение нумерации страниц: ", offset
        Itog = Itog + ParserLet(lists * 4, offset)
        tetra = tetra + 1
    print "Тетрадь №", tetra, " (последняя)"
    print "Смещение нумерации страниц: ", tetra * lists * 4
    Itog = Itog + ParserLet(out_tetr * 4, tetra * lists * 4)
    print "Окончательно последовательность печати страниц следующая: ", Itog

if __name__ == '__main__':
    main()
